"""Test the CameraProjectionMatrix node."""

# Import built-in modules
import unittest

# Import third-party modules
import nuke
import nukescripts


class TestCameraProjectionMatrix(unittest.TestCase):

    def setUp(self):
        """Create the test nodes."""
        self.nodes_before = nuke.allNodes()
        nukescripts.clear_selection_recursive()

        self.cam = nuke.createNode('Camera', 'translate {0 8 6}')
        self.cam['selected'].setValue(False)

        self.axis = nuke.createNode('Axis', 'translate {0 .5 -3}')

        self.reformat = nuke.nodes.Reformat()

        self.cam_matrix_node = nuke.createNode('CameraProjectionMatrix')
        self.cam_matrix_node.setInput(0, self.cam)

        for node in (self.cam, self.axis, self.reformat):
            node.autoplace()

    def tearDown(self):
        """Delete the test nodes."""
        all_nodes = nuke.allNodes()
        added_nodes = set(all_nodes) - set(self.nodes_before)

        for node in list(added_nodes):
            nuke.delete(node)

    @staticmethod
    def _create_reconcile3d(reformat, cam, axis):
        nukescripts.clear_selection_recursive()
        reconcile = nuke.nodes.Reconcile3D()
        reconcile.setInput(0, reformat)
        reconcile.setInput(1, cam)
        reconcile.setInput(2, axis)

        reconcile['calc_output'].setValue(True)

        reconcile.autoplace()
        return reconcile

    def _get_camera_projection_matrix4(self):
        """Return the projection matrix.

        Returns:
            nuke.math.Matrix4: The projection matrix of the test node.

        """
        cam_projection = self.cam_matrix_node['projection'].valueAt(0)
        cam_matrix = nuke.math.Matrix4()
        for index, value in enumerate(cam_projection):
            cam_matrix[index] = value

        return cam_matrix

    def test_matches_reconcile3d(self):
        """A reconciled point matches the output of the Reconcile3D node."""
        # Using a reconcile instead of `nukescripts.snap3d.cameraProjectionMatrix`
        # because the latter is dependend on the root format.
        # With the Reconcile3D node the implementation can be tested against
        # multiple formats without changing the root format.
        reconcile = self._create_reconcile3d(
            self.reformat,
            self.cam,
            self.axis,
        )
        nuke.execute(reconcile, 1, 1)
        ref_point_2d = reconcile['output'].value()
        cam_matrix = self._get_camera_projection_matrix4()

        axis_vector = nuke.math.Vector4(*self.axis['translate'].value() + [1])
        axis_vector = cam_matrix.transform(axis_vector)
        axis_vector /= axis_vector[3]
        self.assertEqual(ref_point_2d, (axis_vector[0], axis_vector[1]))

    def test_matches_nukescripts_snap3d(self):
        """The result matches `nukescripts.snap3d.cameraProjectionMatrix`."""
        ref_matrix = nukescripts.snap3d.cameraProjectionMatrix(self.cam)

        cam_matrix = self._get_camera_projection_matrix4()
        self.assertEqual(cam_matrix, ref_matrix)


def main():
    """Entry point to execute all unittests."""
    nuke.executeInMainThread(unittest.main)

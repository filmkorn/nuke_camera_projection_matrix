//! An Axis node displaying an upstrams camera's projection matrix.

static const char* const CLASS = "CameraProjectionMatrix";
static const char* const HELP = "Shows the attached Camera's projection matrix.";

#include "DDImage/Op.h"
#include "DDImage/CameraOp.h"
#include "DDImage/Convolve.h"
#include "DDImage/Knobs.h"
#include "DDImage/Knob.h"
#include "DDImage/Format.h"

using namespace DD::Image;


class CameraProjectionMatrix : public Op, public ArrayKnobI::ValueProvider
{
    ConvolveArray _projection_array;
    FormatPair _formats;

protected:
    //! Validate the input.
    void _validate(bool for_real) override
    {
        input(0)->validate();
    }

public:
    static const Description description;
    const char* Class() const { return CLASS; }
    const char* node_help() const { return HELP; }

    CameraProjectionMatrix(Node* node) : Op(node)
    {
        _formats.format(0);  // Get default script format.
    }

    //! Set the input's label.
    const char* input_label(int input, char* buffer) const
    {
        return "cam";
    }

    //! Allow to connect to a camera.
    bool test_input(int input, Op* op) const {
        return dynamic_cast<CameraOp*>(op) != 0;
    }

    //! Turn on the value provider.
    bool provideValuesEnabled(const ArrayKnobI*, const OutputContext& oc) const
    {
        return 1;
    }

    //! Calculate the projection matrix.
    std::vector<double> provideValues(const ArrayKnobI*, const OutputContext& oc) const
    {
        // Get the upstream camera.
        CameraOp* upstream_camera = dynamic_cast<CameraOp*>(input(0));
        Matrix4 projection_matrix;

        if (upstream_camera)
        {
            upstream_camera->validate(false);

            // Projection matrix based on the focal length, aperture and clipping planes of the camera.
            int projection_mode = upstream_camera->projection_mode();
            projection_matrix = upstream_camera->projectionAt(oc);

            // For most uses you want to multiply projection()*xform() and pass the result through to_format()
            // to get the transformation from world space to output pixels.
            Format format = *_formats.fullSizeFormat();
            Matrix4 format_matrix;
            format_matrix.makeIdentity();
            upstream_camera->to_format(format_matrix, &format);

            // The cameras world matrix.
            Matrix4 xform;
            upstream_camera->matrixAt(oc, xform);
            xform = xform.inverse();

            projection_matrix = format_matrix * projection_matrix * xform;
            projection_matrix.transpose();
        }

        std::vector<double> values;
        double array[16];
        Matrix4ToArray(projection_matrix, array);
        for (int i = 0; i < 16; i++)
        {
            values.push_back(array[i]);
        }

        return values;
    }

    //! Create the knobs and set the value provider.
    void knobs(Knob_Callback f)
    {
        Format_knob(f, &_formats, "format");
        Knob* array_knob = Array_knob(f, &_projection_array, 4, 4, "projection");
        array_knob->set_flag(Knob::EARLY_STORE);

        if (f.makeKnobs()) {
            dynamic_cast<ArrayKnobI*> (array_knob)->setValueProvider(this);
        }
    }
};


static Op* build(Node* node) { return new CameraProjectionMatrix(node); }
const Op::Description CameraProjectionMatrix::description(CLASS, build);
